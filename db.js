const Sequelize = require('sequelize');

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: 'PATH_TO_YOUR_SQLITE_TOOLS/DB'
});

const connect = async () => {
  try {
    await sequelize.authenticate();
    console.log('connected');

  } catch (err) {
    console.log('connection failed', err);
  }

}

connect();

module.exports = sequelize;
