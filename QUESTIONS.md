# Questions

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

Ans1: The output will be:
```
2
1
```
This is because when the file is read, the setTimeout has ordered a callback in approx. 100 ms, it will be stored on the event loop and the file continues to be read. console.log("2") happens as soon as it is hit, after all following immediate executables the js engine will check for anything waiting in the event queue to be executed and fire them off if so.

Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```
result:
```
10
9
8
7
6
5
4
3
2
1
0
```
This one surprised me, I thought initially it would have been the other way round
However due to the recursion happening inside foo
it is looping inside foo,
creating a new instance of foo with each call (foo (d+1))
moving on to the new instance, the previous doesn't complete, but remembers that it eventually needs to.
once the condition of the loop is met it can console.log 10.
The call stack knows that the other foos haven't returned and now that the conditional loop is not holding things up it can fire them off.
They each remember what d was at the time of their calling because it had never been reassigned.
Each foo have their own lexically scoped d variable.


Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```
Ans3:

Any falsy value will result in d being the default value, which might not be what you intend and could be very unintuitive.
Let's look at the same example, but in a new outfit:
```js
    function addLoanInterest(d) {
      d = d || 5.00;
      statement.adjustLoan(d)
    }
```
If you make a call to addLoanInterest(0) or addLoanInterest(null), you will still be adding 5.00 and an angry call is coming your way.


Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

Ans4:
Output:
```
3
```
This creates a closure. foo assigns the value passed to a (a = 1)
Then returns a function, which gets assigned to bar.
So, bar ultimately is this:
bar = function(b){ return a + b; }
when you make a call to bar, b is assigned the value you pass (b = 2)
but the function of bar isn't just a function, it's a function that remembers the environment that it was created in, which is foo. So it has access to all variables assigned in foo.

the scopes will look like this:
```
global scope [
        foo scope [
            a = 1;
            bar scope [
                b = 2
            ]
        ]
]
```

```
bar: do I have a? no. I let me look outside in foo, yes there it is! Okay, I'll use that;
bar: do I have b? yep. No need to look further.
bar: return result of a + b
```
Printing a in global will result in a ReferenceError: a is not defined, because it doesn't have access to it within it's scope.


Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

Ans5:
The double function is expecting
a value as the first argument,
and a callback as the second argument

```
double(4, (result) => console.log(result));
```
