# Node code challenge

# About this project
The objective was import data to a sqlite local database, create a server which can can be queried for points of interest in the UK and then to render results to the page.

For the backend I've used Express.js and to test I've used Chai and Supertest.

For the frontend, for ease of use, I've used create-react-app and have tested using jest and enzyme.

## Example of Use
A search for 'hastin' will produce a list of:
[
  "Hastings Castle",
  "Hastings Slieve Donard Hotel",
  "Hastings Everglades Hotel",
  "Hastings Railway Station",
  "Hastings Culloden Estate",
  "Hastings Europa Hotel",
  "Hastings District",
  "Hastingleigh",
  "Hastingwood",
  "Hastings Stormont",
  "Hastings Ballygally Castle Hotel",
  "Hastings Culloden Estate & Spa",
  "Hastings Slieve Donard Resort And Spa"
]


# Prerequisites

You should have sqlite downloaded and know the path of the file.
To download, please visit [the sqlite download page](https://www.sqlite.org/download.html) and select the right one for your system.

## Setting up database

1. When you start up sqlite in your terminal give a database name:

```
sqlite3 production
```
2. Once you are running sqlite, create a new table:
```
create table places(geonameid, name, asciiname, alternatenames, latitude, longitude, feature_class, feature_code, country_code, cc2, admin1_code, admin2_code, admin3_code, admin4_code, population, elevation, dem, timezone, modification_date);
```
3. Within this project the data file is with the extension .tsv, so we need to tell it to separate by tabs:
```
.separator "\t"
```
4. Copy the file GB.tsv, found in the data founder, into the location where you saved sqlite:
Place it within the same file as the sqlite exec files.
You should also see your 'production' database made earlier.
Example of file to path:
```
~/Desktop/sqlite-tools-osx-x86-3310100/
```
5. Import file data to table places (created on step 2)
```
.import GB.tsv places
```
If successful you should be able to run a sqlite command to retrieve data (example):
```
SELECT name from places limit 2;
```
6. You will need to change storage to your own path for the database in the db.js file at the root of this folder.
```
const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: '/PATH_TO_YOUR_SQLITE/sqlite-tools-osx-x86-3310100/production'
});

```

# Run tests

## Endpoint tests:
From root of folder:
```
npm run test
```

## React component tests:
```
cd client
npm run test
```

# Running the project

## Backend:
From root of folder:
```
node index.js
```

## Frontend:
On execution this will open a browser tab for you.
```
cd client
npm run start
```
