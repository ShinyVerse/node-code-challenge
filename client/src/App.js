import React, { useState } from 'react';
import SearchBar from './components/SearchBar/SearchBar';
import SearchList from './components/SearchList/SearchList';
import axios from 'axios';
import './App.css';

function App() {
  const [list, setList] = useState([])
  const [renderList, setRenderList] = useState([])
  const [savedQuery, setSavedQuery] = useState("");

  const handleQueryChange = async (query) => {
   if(query.length >= 2) {
      if (savedQuery.length === 0 || !query.startsWith(savedQuery)) {
        setSavedQuery(query);
        const res = await axios.get(`http://localhost:4000/locations?q=${query}`);
        setList(JSON.parse(res.data.msg))
        setRenderList(JSON.parse(res.data.msg));
        return;
      } else {
        query = query.toLowerCase();
         const filteredList = list.filter(item => {
           const itemName = item.name.toLowerCase();
           if(itemName.startsWith(query)) {             return item;
           }
         })
         setRenderList(filteredList);
        return;
      }
   } else {
     setSavedQuery("");
     setList([]);
   }
  }

  return (
    <div className="App">
      <SearchBar onQueryChange={(query) => {handleQueryChange(query)}}/>
      <SearchList list={renderList}/>
    </div>
  );
}

export default App;
