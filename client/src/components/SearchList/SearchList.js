import React from 'react';
import PropTypes from 'prop-types';
import './SearchList.css'

const SearchList = ({list}) => {
    if(list.length === 0) {
      return null;
    }

  return (
    <ul id='search-list'>
      {list.map(item => {
      return <li key={item.geonameid} className="search-result-item">{item.name}<p className='item-let-long'>({item.latitude}, {item.longitude})</p></li>
      })}
    </ul>
  )
}

SearchList.propTypes = {
  list: PropTypes.array
}

export default SearchList
