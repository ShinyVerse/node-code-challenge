import React from 'react';
import { shallow } from 'enzyme';
import SearchList from './SearchList';

describe('SearchList', () => {
  let wrapper;

  test('renders nothing if list empty', () => {
    wrapper = shallow(<SearchList list={[]}/>);
    const searchList = wrapper.find('#search-list');

    expect(searchList.length).toBe(0);
  })

  test('renders to the page when populated list present', () => {
    const list = [{geonameid: 'search-item-1', name: 'item1'}, {geonameid: 'search-item-2', name: 'item2'}]
    wrapper = shallow(<SearchList list={list}/>);
    const searchList = wrapper.find('#search-list');

    expect(searchList.length).toBe(1);
  })

  test('renders result list items to the page', () => {
    const list = [
      {geonameid: 'search-item-1', name: 'item1', latitude: 20.00, longitude: 3.00},
      {geonameid: 'search-item-2', name: 'item2', latitude: 30.00, longitude: 4.00}]

    wrapper = shallow(<SearchList list={list}/>);

    const searchList = wrapper.find('#search-list');

    const itemsNodeList = searchList.find('.search-result-item');

    expect(itemsNodeList.length).toBe(2);

    itemsNodeList.forEach((element, index) => {
      expect(element.text()).toBe(list[index].name + `(${list[index].latitude}, ${list[index].longitude})`)
    });

  })
})
