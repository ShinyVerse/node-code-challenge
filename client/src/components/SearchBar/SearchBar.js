import React, { useState, Fragment } from 'react'
import PropTypes from 'prop-types';
import './SearchBar.css';

const SearchBar = ({onQueryChange}) => {
  const [query, setQuery] = useState('');

  return (
    <Fragment>
      <label id='search-bar-label' htmlFor='search-bar'>Search:</label>
      <input
        type="text"
        id='search-bar'
        name='search-bar'
        value={query}
        onChange={(event) => {setQuery(event.target.value); onQueryChange(event.target.value)}}
        />
    </Fragment>

  )
}

SearchBar.propTypes = {
  onQueryChange: PropTypes.func.isRequired,
}

export default SearchBar;
