import React from 'react';
import { shallow } from 'enzyme';
import SearchBar from './SearchBar';

describe('SearchBar', () => {
  let wrapper;
  let onQueryChangeSpy;

  beforeEach(() => {
    onQueryChangeSpy = jest.fn();
    wrapper = shallow(<SearchBar onQueryChange={(query) => onQueryChangeSpy(query)} />);
  });

  test('renders searchbar to the page', () => {
    const searchBar = wrapper.find('#search-bar');

    expect(searchBar.length).toBe(1);

  })

  test('renders a label to the page', () => {
    const searchBarLabel = wrapper.find('#search-bar-label');

    expect(searchBarLabel.length).toBe(1);
    expect(searchBarLabel.text()).toBe('Search:')
  })

  test('search bar has initial value of ""', () => {
    const searchBar = wrapper.find('#search-bar');

    expect(searchBar.props().value).toBe('');
  })


  test('can change value of query', () => {
    let searchBar = wrapper.find('#search-bar');

    searchBar.simulate('change', { target: { value: 'newValue' } });
    searchBar = wrapper.find('#search-bar');

    expect(searchBar.props().value).toBe('newValue');
  })

  test('search value change triggers call to queryChange with query', () => {
    let searchBar = wrapper.find('#search-bar');
    const newValue = 'newValue';

    searchBar.simulate('change', { target: { value: newValue } })

    expect(onQueryChangeSpy).toHaveBeenCalledWith(newValue);
  })
})
