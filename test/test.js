const expect = require('chai').expect;
const request = require('supertest');
const app = require('../server');

describe('API endpoints', () => {
  let server;

  beforeEach(() => {
    server = app.listen(8080);
  })

  afterEach(() => {
    server.close();
  })

  describe('GET /locations?:q', () => {
    it('should receive single result from query', (done) => {
      const expectedResult = [{"geonameid":"2637162","name":"Loch na Sreinge","asciiname":"Loch na Sreinge","alternatenames":"Loch Sreinge,Loch na Sreinge","latitude":"56.28333","longitude":"-5.35","feature_class":"H","feature_code":"LK","country_code":"GB","cc2":"GB","admin1_code":"SCT","admin2_code":"T8","admin3_code":"","admin4_code":"","population":"0","elevation":"","dem":"184","timezone":"Europe/London","modification_date":"2012-01-17"}];

      const result = { msg: JSON.stringify(expectedResult) };
        request(app)
        .get('/locations?q=Loch na Sreinge')
        .end((err, res) => {
          expect(res.body).to.deep.equal(result);
          done();
        })
      })

      it('should receive multiple results from query', (done) => {
        request(app)
        .get('/locations?q=Hastin')
        .end((err, res) => {
          const firstResultName = 'Hastings Shoals';
          const lastResultName = 'Hastings Slieve Donard Resort And Spa';
          const msgBody = JSON.parse(res.body.msg)
          expect(msgBody.length).to.equal(16);
          expect(msgBody[0].name).to.equal(firstResultName);
          expect(msgBody[15].name).to.equal(lastResultName);
          done();
        })
      })

      it('should return empty when no results are found', (done) => {
        request(app)
        .get('/locations?q=thisIsNotGoingToBeThere')
        .end((err, res) => {
          const msgBody = JSON.parse(res.body.msg)
          expect(msgBody.length).to.equal(0);
          done();
        })
      })
    })
  })
